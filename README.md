# app

A Simple Admin Panel


### Features: 

1.	A simple admin panel with laravel 5.5 environment
2.	Authentication With Role-Permission Management
3.	Admin can manage role-permissions
4.	User management system
5.	Admin can manage user module like create-read-update-delete
6.	Multiple role assign to users
7.	Multiple permissions assign to role
8.	User management maintain with Vue JS Axios Api 


## Technology

1.	Programming Language: PHP >7.2
2.	Web Framework: Laravel >5.5
3.	Database: MySQL
4.	Front End Design: HTML5, CSS3, Bootstrap, Vue JS

 
# Installation
First clone the project. Then run
 
	 composer update 
	 OR 
	 composer install
 
Depending on your OS this command may be in different format.

## Configuration
Than you can create your .env file as it is in [Laravel 5 documentation](http://laravel.com/docs/master) or can use this sample:
 
 APP_ENV=local
 APP_DEBUG=true
 APP_KEY=your_key_here 

 DB_HOST=db_host
 DB_DATABASE=database_name
 DB_USERNAME=database_user
 DB_PASSWORD=database_password

 CACHE_DRIVER=array
 SESSION_DRIVER=file

 EMAIL_ADDRESS=application_email@domain.com
 EMAIL_PASSWORD=email_password

Put your database host, username and password. ```EMAIL_ADDRESS``` is the application mailing service address. ```EMAIL_PASSWORD``` is the password for the mailbox. I am using this way of configuration due to the mail.php config file commit. I do not want to distribute my email and password ;).

For more details about the .env file, check [Laravel's documentation](http://laravel.com/docs/master) or just Google about it. There is a plenty of info out there.
 
## Run API Key Generate
 
	 php artisan key:generate


## Run the migrations
First create your database and set the proper driver in the ```config/database.php``` file.
Use the Laravel's artisan console with the common commands to run the migrations. First cd to the project directory and depending from your OS run 
 
	 php artisan migrate

 
 
## Add some dummy data
This project has seeders which provide the initial and some dummy data necessary for the project to run.
Use: 
 
	 php artisan db:seed
	  OR 
	 php artisan migrate:refresh -seed
 
to run the migrations.


## Autoload php files

	 composer dump-autoload
 
 
## Clear Cache and Storage
 
	 php artisan config:clear
	 php artisan cache:clear
 
## Install the frontend dependencies using the following command.
 
	 npm install
	 npm run dev
	 
## Your first login
 
	Email: admin@domain.com
	Password: 123456

### New Users default password is - 123456

## Contributing

Md Anisur Rahman

Web Programmer


http://anis.wdpfr36.website/

AS Well AS

Web Programmer 
R-creation 
Chattogram, Bangladesh.
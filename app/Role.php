<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    public function getDisplayNameAttribute($display_name)
    {
        return ucwords($display_name);
    }

    // public function permissions(){
    //     return $this->hasMany(Permission::class);
    //  }



    
    
}

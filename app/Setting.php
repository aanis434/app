<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['app_name','app_logo','address','address_02','phone','email','about'];


    // public function getAppLogoAttribute($app_logo)
    // {
    //     return asset($app_logo);
    // }

    public function getAppNameAttribute($app_name)
    {
        return ucwords($app_name);
    }
    
}

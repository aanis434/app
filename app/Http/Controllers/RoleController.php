<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Setting;

use Session;
use Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(!Auth::user()->hasRole('admin'))
        {
            Session::flash('info','You must have admin role to read this.');
            return redirect()->back(); 
        }
        return view('role.list')
            ->with('roles',Role::all())
            ->with('settings',Setting::first()); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->hasRole('admin') && !Auth::user()->can(['create']))
        {
            Session::flash('info','You must have Permission to create.');
            return redirect()->back();
        }
        return view('role.create')->with('settings',Setting::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "name" => "required",
            "display_name" => "required",
        ]);

        $role = Role::create([
            'name' => $request->name,
            'display_name' => $request->display_name,
            'description' => $request->description,
        ]);

        Session::flash('success','Role successfully created.');

        return redirect()->route('role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->hasRole('admin') && !Auth::user()->can(['edit']))
        {
            Session::flash('info','You must have Permission to edit.');
            return redirect()->back();
        }

        $role = Role::find($id);

        return view('role.edit')->with('role',$role)->with('settings',Setting::first()); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            "name" => "required",
            "display_name" => "required",
        ]);

        $role = Role::find($id);


        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;

        $role->save();

        Session::flash('success','Role successfully updated.');

        return redirect()->route('role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->hasRole('admin') && !Auth::user()->can(['delete']))
        {
            Session::flash('info','You must have Permission to delete.');
            return redirect()->back();
        }
        $roles = Role::findOrFail($id);
        $roles->delete();

        //dd($role);
        // Force Delete
        $roles->users()->sync([]);
        // Delete relationship data
        $roles->perms()->sync([]); // Delete relationship data

        $roles->forceDelete(); // Now force delete will work regardless of whether the pivot table has cascading delete

        Session::flash('info','Role successfully deleted.');
        return redirect()->back();
    }
}

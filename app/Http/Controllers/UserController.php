<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserCollection;
use App\User;
use App\Role;
use Session;
use Auth;

class UserController extends Controller
{
    public function store(Request $request)
    {
      $user = new User([
        'name' => $request->get('name'),
        'email' => $request->get('email'),
        'password' => bcrypt('123456')
      ]);

      $user->save();
      $role_id = $request->get('role_id');

      if(count($role_id)>0){
        foreach($role_id as $id){
          $user->roles()->attach(array($id));
        }
      }

      return response()->json('success');

    }

    public function index()
    {
      return new UserCollection(User::with('roles')->get());
    }

    public function get_roles()
    {
        return new UserCollection(Role::all());
    }

    public function edit($id)
    {
      $user = User::with('roles')->find($id);
      return response()->json($user);
    }

    public function update($id, Request $request)
    {
      $user = User::with('roles')->find($id);

      $user->name = $request->get('name');
      $user->email = $request->get('email');

      $user->update();
      $roles = $request->get('roles');
      $user->roles()->sync($roles);

      return response()->json('success');
    }

    public function delete($id)
    {

      $user = User::with('roles')->find($id);

      $user->delete();

      $user->roles()->sync([]);

      $user->forceDelete();

      return response()->json('success');
    }

    public function remove_user_role($user_id,$role_id)
    {
      //dd($role_id);
      $user = User::with('roles')->find($user_id);
      $user->detachRole($role_id);

      return response()->json('success');
    }
}

<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use App\Setting;

use Session;
use Auth;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(!Auth::user()->hasRole('admin'))
        {
            Session::flash('info','You must have admin role to read this.');
            return redirect()->back(); 
        }

        return view('permission.list')
            ->with('permission',Permission::all())
            ->with('settings',Setting::first()); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->hasRole('admin') && !Auth::user()->can(['create']))
        {
            Session::flash('info','You must have Permission to create.');
            return redirect()->back();
        }
        return view('permission.create')->with('settings',Setting::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "name" => "required",
            "display_name" => "required",
        ]);

        $permission = Permission::create([
            'name' => $request->name,
            'display_name' => $request->display_name,
            'description' => $request->description,
        ]);

        Session::flash('success','New Permission successfully created.');

        return redirect()->route('permission');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->hasRole('admin') && !Auth::user()->can(['edit']))
        {
            Session::flash('info','You must have Permission to edit.');
            return redirect()->back();
        }

        $permission = Permission::find($id);

        return view('permission.edit')->with('permission',$permission)->with('settings',Setting::first()); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            "name" => "required",
            "display_name" => "required",
        ]);

        $permission = Permission::find($id);


        $permission->name = $request->name;
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;

        $permission->save();

        Session::flash('success','Permission successfully updated.');

        return redirect()->route('permission');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->hasRole('admin') && !Auth::user()->can(['delete']))
        {
            Session::flash('info','You must have Permission to delete.');
            return redirect()->back();
        }
        $permission = Permission::where('id','=',$id)->first();
        $permission->delete();

        Session::flash('info','Permission successfully deleted.');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Permission;
use App\Setting;

use Session;
use Auth;

class PermissionRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(!Auth::user()->hasRole('admin'))
        {
            Session::flash('info','You must have admin role to read this.');
            return redirect()->back(); 
        }

        $roles = Role::with('perms')->get();
        
        return view('permission-role.list')
            ->with('role',$roles)
            ->with('settings',Setting::first()); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->hasRole('admin') && !Auth::user()->can(['create']))
        {
            Session::flash('info','You must have Permission to attach.');
            return redirect()->back();
        }
        return view('permission-role.create')
            ->with('roles',Role::all())
            ->with('perms',Permission::all())
            ->with('settings',Setting::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "role_id" => "required",
            "perm_id" => "required",
        ]);

        $role_id = $request->role_id;
        $perm_id = $request->perm_id;
        //dd($perm_id);

        $role = Role::find($role_id);
        $role->perms()->sync($perm_id);

        Session::flash('success','Permission successfully attached.');

        return redirect()->route('permission-role');
       
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Detach Permission from a Role 
    public function destroy($role_id,$perm_id)
    {
        if(!Auth::user()->hasRole('admin') && !Auth::user()->can(['delete']))
        {
            Session::flash('info','You must have Permission to delete.');
            return redirect()->back();
        }
        
        $role = Role::with('perms')->find($role_id);
        $role->detachPermission($perm_id);
      
        Session::flash('info','Permission remove successfully.');
        return redirect()->back();
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex page-header">
        <h2 class="page-title">
            Dashboard
        </h2>
        <div class="d-flex order-lg-2 ml-auto">
            <div class="nav-item d-none d-md-flex">
                @if(Session::has('info'))
                <div class="alert alert-icon alert-primary alert-dismissible" role="alert">
                    <i class="fe fe-bell mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert"></button>
                    {{ Session::get('info') }} 
                  </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-icon alert-success alert-dismissible" role="alert">
                    <i class="fe fe-check mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert"></button>
                    {{ Session::get('success') }} 
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                    <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert">
                    {{ Session::get('error') }}
                </div>
                @endif
            </div>
        </div>
    </div>
    @if(Auth::user()->hasRole(['superadmin', 'admin']))
    <div class="row row-cards">
        <div class="col-sm-6 col-lg-3">
        <div class="card p-3">
            <div class="d-flex align-items-center">
            <span class="stamp stamp-md bg-blue mr-3">
                <i class="fe fe-user"></i>
            </span>
            <div>
                <h4 class="m-0"><a href="{{ url('/role') }}">{{ !empty($roles)?$roles:'132' }} <small>Roles</small></a></h4>
                <small class="text-muted">Show Roles list.</small>
            </div>
            </div>
        </div>
        </div>
        <div class="col-sm-6 col-lg-3">
        <div class="card p-3">
            <div class="d-flex align-items-center">
            <span class="stamp stamp-md bg-green mr-3">
                <i class="fe fe-user-check"></i>
            </span>
            <div>
                <h4 class="m-0"><a href="{{ url('/permission') }}">{{ !empty($permissions)?$permissions:'32' }} <small>Permissions</small></a></h4>
                <small class="text-muted">Show Permissions list.</small>
            </div>
            </div>
        </div>
        </div>
        <div class="col-sm-6 col-lg-3">
        <div class="card p-3">
            <div class="d-flex align-items-center">
            <span class="stamp stamp-md bg-red mr-3">
                <i class="fe fe-users"></i>
            </span>
            <div>
                <h4 class="m-0"><a href="{{ url('/user/list') }}">{{ !empty($members)?$members:'1,352' }} <small>Members</small></a></h4>
                <small class="text-muted">Show all registered member.</small>
            </div>
            </div>
        </div>
        </div>
        <div class="col-sm-6 col-lg-3">
        <div class="card p-3">
            <div class="d-flex align-items-center">
            <span class="stamp stamp-md bg-yellow mr-3">
                <i class="fe fe-message-square"></i>
            </span>
            <div>
                <h4 class="m-0"><a href="javascript:void(0)">132 <small>Message</small></a></h4>
                <small class="text-muted">16 waiting</small>
            </div>
            </div>
        </div>
        </div>
    </div>
    @else
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-primary">Hello User? <a href="javascript:void" class="alert-link"> Welcome</a> to user panel.</div>
        </div>
    </div>
    @endif
</div>
@endsection

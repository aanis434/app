@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex page-header">
        <h1 class="page-title">
        <a href="{{ route('permission-role-create') }}" class="btn btn-sm btn-info">
                <i class="fe fe-plus-square"></i> Attach Permission</a>
        </h1>
        <div class="d-flex order-lg-2 ml-auto">
            <div class="nav-item d-none d-md-flex">
                @if(Session::has('info'))
                <div class="alert alert-icon alert-primary alert-dismissible" role="alert">
                    <i class="fe fe-bell mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert"></button>
                    {{ Session::get('info') }} 
                    </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-icon alert-success alert-dismissible" role="alert">
                    <i class="fe fe-check mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert"></button>
                    {{ Session::get('success') }} 
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                    <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert">
                    {{ Session::get('error') }}
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Permission-Role</h3>
            </div>
            <div class="table-responsive">
            <table class="table card-table table-vcenter text-nowrap datatable">
                <thead>
                <tr>
                    <th class="w-1">#Sl No.</th>
                    <th>Role</th>
                    <th>Permission</th>
                    {{-- <th>Actions</th> --}}
                </tr>
                </thead>
                <tbody>
                    <?php $key = 0; ?> 
                    @for($i=0;$i<count($role);$i++)
                    <?php 
                    $perms = $role[$i]['perms'];
                    $key++;
                    ?>
                    @if(!empty($perms))
                    <tr>
                        <td><span class="text-muted">{{ $key }}</span></td>
                        <td>{{ $role[$i]['name'] }}</td>
                        <td>
                            @foreach($perms as $value)
                                [{{ $value['name'] }} <a class="icon" onclick="return confirm_delete()" title="Remove Permission" href="{{ route('permission-role-destroy',['role_id'=>$role[$i]['id'],'perm_id'=>$value['id']]) }}">
                                    <i class="fe fe-trash"></i> 
                                </a> ]&nbsp;
                            @endforeach

                        </td>
                        {{-- <td>
                        @if($permission_role[$i]['name'] !='admin')
                            <a class="icon" href="{{ route('permission-role-edit',['id'=>$role[$i]['id']]) }}">
                                <i class="fe fe-edit"></i>
                            </a>&nbsp;
                            <a class="icon" href="{{ route('permission-role-destroy',['id'=>$role[$i]['id']]) }}">
                                <i class="fe fe-trash"></i> 
                            </a>
                        @endif
                        </td> --}}
                    </tr>
                    @endif
                    @endfor
                </tbody>
            </table>
            
            </div>
        </div>
        </div>
    </div>
</div>
@stop

@section('scripts')

    <script>
        require(['datatables', 'jquery'], function(datatable, $) {
                $('.datatable').DataTable();
            });

        
        function confirm_delete() { return confirm('Are you sure to remove the permission?'); }
        </script>

@stop
<!doctype html>
<html lang="{{ app()->getLocale() }}">
  {{-- Header add --}}
  @include('includes.header')
  <body class="">
      <div class="page">
        <div class="flex-fill">
           {{-- Topbar add --}}
          @include('includes.topbar')

  
          @if (Auth::check())
            {{-- Header Menu add --}}
            @include('includes.header-menu')
          @endif

          <div class="my-3 my-md-5">
             {{--Page Content add --}}
              @yield('content')
          </div>

        </div>
          {{-- Footer add --}}
          @include('includes.footer')

      </div>
  </body>
</html>
     
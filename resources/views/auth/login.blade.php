@extends('layouts.app')

@section('content')
    <div class="page">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              {{-- <div class="text-center mb-6">
                <img src="./demo/logo.svg" class="h-6" alt="">
              </div> --}}
              <form class="card" method="POST" action="{{ route('login') }}">
                  {{ csrf_field() }}
                <div class="card-body p-6">
                  <div class="card-title">Login to your account</div>
                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="form-label">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="{{ old('email') }}" required autofocus autocomplete="off">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="form-label">
                      Password
                      <a href="{{ route('password.request') }}" class="float-right small">I forgot password</a>
                    </label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required autocomplete="off">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" name="remember" class="custom-control-input" />
                      <span class="custom-control-label" {{ old('remember') ? 'checked' : '' }}>Remember me</span>
                    </label>
                  </div>
                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                  </div>
                </div>
              </form>
              {{-- <div class="text-center text-muted">
                Don't have account yet? <a href="./register.html">Sign up</a>
              </div> --}}
            </div>
          </div>
        </div>
      </div>
    </div>
    
@endsection
 {{-- <div class="footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-8">
              <div class="row">
                <div class="col-6 col-md-3">
                  <ul class="list-unstyled mb-0">
                    <li><a href="#">First link</a></li>
                    <li><a href="#">Second link</a></li>
                  </ul>
                </div>
                <div class="col-6 col-md-3">
                  <ul class="list-unstyled mb-0">
                    <li><a href="#">Third link</a></li>
                    <li><a href="#">Fourth link</a></li>
                  </ul>
                </div>
                <div class="col-6 col-md-3">
                  <ul class="list-unstyled mb-0">
                    <li><a href="#">Fifth link</a></li>
                    <li><a href="#">Sixth link</a></li>
                  </ul>
                </div>
                <div class="col-6 col-md-3">
                  <ul class="list-unstyled mb-0">
                    <li><a href="#">Other link</a></li>
                    <li><a href="#">Last link</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-4 mt-4 mt-lg-0">
              Premium and Open Source dashboard template with responsive and high quality UI. For Free!
            </div>
          </div>
        </div>
      </div> --}}
      <footer class="footer">
            <div class="container">
              <div class="row align-items-center flex-row-reverse">
                <div class="col-auto ml-lg-auto">
                  <div class="row align-items-center">
                    <div class="col-auto">
                      <ul class="list-inline list-inline-dots mb-0">
                        <li class="list-inline-item"><a href="javascript:void">Documentation</a></li>
                      </ul>
                    </div>
                    <div class="col-auto">
                      <a href="javascript:void" class="btn btn-outline-primary btn-sm">Source code</a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
                    Copyright © 2019. Developed by <a href=".">Md. Anisur Rahman</a> All rights reserved.
                </div>
              </div>
            </div>
          </footer>
          
        <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/require.min.js') }}"></script>
        <script>
          requirejs.config({
              baseUrl: '/'
          });
        </script>


        <!-- Dashboard Core -->
        <script src="{{ asset('assets/js/dashboard.js') }}"></script>

         

        <!-- c3.js Charts Plugin -->
        <link href="{{ asset('assets/plugins/charts-c3/plugin.css') }}" rel="stylesheet" />
        <script src="{{ asset('assets/plugins/charts-c3/plugin.js') }}"></script>
        <!-- Google Maps Plugin -->
        <link href="{{ asset('assets/plugins/maps-google/plugin.css') }}" rel="stylesheet" />
        <script src="{{ asset('assets/plugins/maps-google/plugin.js') }}"></script>
        <!-- Input Mask Plugin -->
        <script src="{{ asset('assets/plugins/input-mask/plugin.js') }}"></script>
        <!-- Datatables Plugin -->
        <script src="{{ asset('assets/plugins/datatables/plugin.js') }}"></script>
       

       {{-- page-specific script add  --}}
       @yield('scripts')


       
        
      

    
        
      
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex page-header">
        <h1 class="page-title">
        <a href="{{ route('role-create') }}" class="btn btn-sm btn-info">
                <i class="fe fe-plus-square"></i> Create New Role</a>
        </h1>
        <div class="d-flex order-lg-2 ml-auto">
            <div class="nav-item d-none d-md-flex">
                @if(Session::has('info'))
                <div class="alert alert-icon alert-primary alert-dismissible" role="alert">
                    <i class="fe fe-bell mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert"></button>
                    {{ Session::get('info') }} 
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-icon alert-success alert-dismissible" role="alert">
                    <i class="fe fe-check mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert"></button>
                    {{ Session::get('success') }} 
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                    <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert">
                    {{ Session::get('error') }}
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">All Roles</h3>
            </div>
            <div class="table-responsive">
            <table class="table card-table table-vcenter text-nowrap datatable">
                <thead>
                <tr>
                    <th class="w-1">No.</th>
                    <th>Role Name</th>
                    <th>Display Name</th>
                    <th>Descriptions</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($roles as $key=>$value)
                    <tr>
                        <td><span class="text-muted">{{ $key+1 }}</span></td>
                        <td>{{ $value['name'] }}</td>
                        <td>{{ $value['display_name'] }}</td>
                        <td>{!! $value['description'] !!}</td>
                        <td>
                        @if($value['name'] !='admin')
                            <a class="icon" href="{{ route('role-edit',['id'=>$value['id']]) }}">
                                <i class="fe fe-edit"></i>
                            </a>&nbsp;
                            <a class="icon" onclick="return confirm_delete()" title="Delete Role" href="{{ route('role-destroy',['id'=>$value['id']]) }}">
                                <i class="fe fe-trash"></i>
                            </a>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            </div>
        </div>
        </div>
    </div>
</div>
@stop

@section('scripts')

    <script>
        require(['datatables', 'jquery'], function(datatable, $) {
                $('.datatable').DataTable();
            });

            function confirm_delete() { return confirm('Are you sure to delete the Role?\r\nNote: It is a cascading delete!'); }

    </script>

@stop
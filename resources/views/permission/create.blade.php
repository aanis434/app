@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex page-header">
        <h1 class="page-title">
            {{-- <a href="" class="btn btn-sm btn-info"><i class="fe fe-plus-square"></i> Create New Role</a> --}}
            Create New Permission
        </h1>
        <div class="d-flex order-lg-2 ml-auto">
            <div class="nav-item d-none d-md-flex">
                @if(Session::has('info'))
                <div class="alert alert-icon alert-primary alert-dismissible" role="alert">
                    <i class="fe fe-bell mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert"></button>
                    {{ Session::get('info') }} 
                    </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-icon alert-success alert-dismissible" role="alert">
                    <i class="fe fe-check mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert"></button>
                    {{ Session::get('success') }} 
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                    <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> 
                    <button type="button" class="close" data-dismiss="alert">
                    {{ Session::get('error') }}
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5 col-sm-offset-3">
            
            {{-- validation error alert  --}}
            @if(count($errors)>0)
            <ul class="list-group">
                @foreach ($errors->all() as $error)
                    <li class="list-group-item alert alert-icon alert-danger" role="alert">
                        <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i>
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
            @endif
            
            <form action="{{ route('permission-store') }}" method="POST">
                    {{ csrf_field() }}
                <div class="form-group">
                    <label class="form-label">Name<span class="form-required">*</span></label>
                    <input type="text" class="form-control" name="name"/>
                </div>
                <div class="form-group">
                    <label class="form-label">Display Name<span class="form-required">*</span></label>
                    <input type="text" class="form-control" name="display_name"/>
                </div>
                <div class="form-group">
                    <label class="form-label">Description<span class="form-required"></span></label>
                    <textarea name="description" class="form-control" id="editor" cols="30" rows="3"></textarea>
                </div>
                <div class="form-group text-center">
                    <input type="submit" class="btn btn-sm btn-primary" value="Save">
                    &nbsp;
                <a href="{{ url('/permission') }}" class="btn btn-sm btn-info">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('scripts')
    {{-- ckeditor add  --}}
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
@stop
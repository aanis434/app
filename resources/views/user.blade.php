<!doctype html>
<html lang="{{ app()->getLocale() }}">
  {{-- Header add --}}
  @include('includes.header')
  <body class="">
      <div class="page">
        <div class="flex-fill">
           {{-- Topbar add --}}
          @include('includes.topbar')

  
          @if (Auth::check())
            {{-- Header Menu add --}}
            @include('includes.header-menu')
          @endif

        <div class="my-3 my-md-5">
            <div class="container">
                {{-- <div class="d-flex page-header">
                    <h2 class="page-title">
                        Users List
                    </h2>
                    <div class="d-flex order-lg-2 ml-auto">
                        <div class="nav-item d-none d-md-flex">
                            @if(Session::has('info'))
                            <div class="alert alert-icon alert-primary alert-dismissible" role="alert">
                                <i class="fe fe-bell mr-2" aria-hidden="true"></i> 
                                <button type="button" class="close" data-dismiss="alert"></button>
                                {{ Session::get('info') }} 
                            </div>
                            @endif
                            @if(Session::has('success'))
                            <div class="alert alert-icon alert-success alert-dismissible" role="alert">
                                <i class="fe fe-check mr-2" aria-hidden="true"></i> 
                                <button type="button" class="close" data-dismiss="alert"></button>
                                {{ Session::get('success') }} 
                            </div>
                            @endif
                            @if(Session::has('error'))
                            <div class="alert alert-icon alert-danger alert-dismissible" role="alert">
                                <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> 
                                <button type="button" class="close" data-dismiss="alert">
                                {{ Session::get('error') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div> --}}
                <div class="row row-cards">
                    <div id="app">
                        <example-component></example-component>
                    </div>
                </div>
            </div>
        </div>
    </div>
      {{-- Footer add --}}
      @include('includes.footer')

  </div>
</body>
</html>
 

// app.js

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);

import HomeComponent from './components/HomeComponent.vue';
import CreateComponent from './components/CreateComponent.vue';
import ListComponent from './components/ListComponent.vue';
import EditComponent from './components/EditComponent.vue';

const routes = [
  {
      name: 'user',
      path: '/user',
      component: HomeComponent
  },
  {
      name: 'user-create',
      path: '/user/create',
      component: CreateComponent
  },
  {
      name: 'user-list',
      path: '/user/list',
      component: ListComponent
  },
  {
      name: 'user-edit',
      path: '/user/edit/:id',
      component: EditComponent
  }
];

const router = new VueRouter({ mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');

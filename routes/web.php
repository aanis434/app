<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('home');

# =========================================
# Laravel Proxy route for vue Route Start

Route::get('/user', function () {
    return redirect('/user/list');
  });

Route::get('/user/{any}', function () {
        return view('user');
      })->where('any', '.*');

# Laravel Proxy route for vue Route End
# =========================================

Auth::routes();

Route::get('/role', [
    'uses' => 'RoleController@index',
    'as' => 'role'
]);

Route::get('/role/create',[
    'uses' => 'RoleController@create',
    'as' => 'role-create'
    ]);

Route::post('/role/store',[
    'uses' => 'RoleController@store',
    'as' => 'role-store'
    ]);

Route::get('/role/edit/{id}',[
    'uses' => 'RoleController@edit',
    'as' => 'role-edit'
]);

Route::post('/role/update/{id}',[
    'uses' => 'RoleController@update',
    'as' => 'role-update'
]);

Route::get('/role/destroy/{id}',[
    'uses' => 'RoleController@destroy',
    'as' => 'role-destroy'
]);

Route::get('/permission', [
    'uses' => 'PermissionController@index',
    'as' => 'permission'
]);

Route::get('/permission/create',[
    'uses' => 'PermissionController@create',
    'as' => 'permission-create'
    ]);

Route::post('/permission/store',[
    'uses' => 'PermissionController@store',
    'as' => 'permission-store'
    ]);

Route::get('/permission/edit/{id}',[
    'uses' => 'PermissionController@edit',
    'as' => 'permission-edit'
]);

Route::post('/permission/update/{id}',[
    'uses' => 'PermissionController@update',
    'as' => 'permission-update'
]);

Route::get('/permission/destroy/{id}',[
    'uses' => 'PermissionController@destroy',
    'as' => 'permission-destroy'
]);


Route::get('/permission/role', [
    'uses' => 'PermissionRoleController@index',
    'as' => 'permission-role'
]);

Route::get('/permission/role/create',[
    'uses' => 'PermissionRoleController@create',
    'as' => 'permission-role-create'
    ]);

Route::post('/permission/role/store',[
    'uses' => 'PermissionRoleController@store',
    'as' => 'permission-role-store'
    ]);

Route::get('/permission/role/edit/{id}',[
    'uses' => 'PermissionRoleController@edit',
    'as' => 'permission-role-edit'
]);

Route::post('/permission/role/update/{id}',[
    'uses' => 'PermissionRoleController@update',
    'as' => 'permission-role-update'
]);

Route::get('/permission/role/destroy/{role_id}/{perm_id}',[
    'uses' => 'PermissionRoleController@destroy',
    'as' => 'permission-role-destroy'
]);




// Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){

// });


// Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {
//     Route::get('/', 'AdminController@welcome');
//     Route::get('/manage', [
//             'middleware' => ['permission:manage-admins'], 
//             'uses' => 'AdminController@manageAdmins'
//             ]);
// });
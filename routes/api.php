<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/users/create', 'UserController@store');
Route::get('/users/edit/{id}', 'UserController@edit');
Route::post('/users/update/{id}', 'UserController@update');
Route::delete('/users/delete/{id}', 'UserController@delete');
Route::get('/remove_user_role/{user_id}/{role_id}', 'UserController@remove_user_role');
Route::get('/users', 'UserController@index');
Route::get('/get_roles', 'UserController@get_roles');
//Route::get('/remove_user_role', 'UserController@remove_user_role');

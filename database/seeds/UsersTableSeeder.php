<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'Mr Admin',
            'email' => 'admin@domain.com',
            'password' => bcrypt('123456')
        ]);

        $admin = App\Role::create([
            'name' => 'admin',
            'display_name' => 'Administrator', // optional
            'description'  => 'Admin is allowed to manage and edit and delete other users'
        ]);

        $user->roles()->attach($admin->id);

        $create = App\Permission::create([
            'name' => 'create',
            'display_name' => 'Create Permission', // optional
            'description'  => 'Create permission' // optional
        ]);

        $edit = App\Permission::create([
            'name' => 'edit',
            'display_name' => 'Edit Permission', // optional
            'description'  => 'Edit permission' // optional
        ]);

        $admin->attachPermissions(array($create, $edit));
        // equivalent to $owner->perms()->sync(array($createPost->id, $editUser->id));

        $setting = App\Setting::create([
            'app_name' => 'Simple Admin Panel',
            'email' => 'app@domain.com',
            'phone' => '+88-032-486783',
            'address' => 'Chattogram, Bangladesh',
            'about' => 'Qolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibham
            liber tempor cum soluta nobis eleifend option congue nihil uarta decima et quinta.
            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat eleifend option nihil. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius parum claram.'
        ]);

    }
}
